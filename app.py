# ----- Flask -----
from flask import Flask, jsonify
from flask_cors import cross_origin, CORS

# ----- Keras -----
import numpy as np
import tensorflow as tf
from tensorflow import keras

# ----- Tools -----
import time
from datetime import datetime
import matplotlib.pyplot as plt
from scipy import stats

# ----- Custom -----
from model import Parking, Database

app = Flask(__name__)
CORS(app, allow_headers='Content-Type')

@app.route('/<id>', methods=['GET'])
@cross_origin()
def getById(id):

    db = Database()
    conn = db.connection

    cur = conn.cursor()

    # ----- ESTIMATE -----

    dateFull = datetime.today().strftime('%Y-%m-%d %H:%M:%S')
    date = datetime.today().strftime('%m-%d ') + str(int(datetime.today().strftime('%H')) + 1) + ":00:00" if int(datetime.today().strftime('%H')) + 1 > 9 else + datetime.today().strftime('%m-%d ') + "0" + str(int(datetime.today().strftime('%H')) + 1) + ":00:00"
    dateSearching = "%" + date + "%"

    data = [id, dateSearching]

    cur.execute("SELECT * FROM remplissage WHERE idparking = %s AND date::text LIKE %s", data)

    result = cur.fetchone()

    if result is None:
        return jsonify({'code': 404, 'message' : 'There is no data found for this id!'})

    x = []
    y = []

    for i in cur:
        x.append(int(time.mktime(datetime.strptime(str(i[1]), "%Y-%m-%d %H:%M:%S").timetuple())))
        y.append(int(i[2]))

    x = np.array(x)
    y = np.array(y)

    # plt.plot(x, y, 'ro')

    # plt.show()

    slope, intercept, r_value, p_value, std_err = stats.linregress(x, y)

    fitLine = predict(x, slope, intercept)
    # plt.plot(x, fitLine, c='r')

    estimate = round(predict(time.mktime(datetime.strptime(dateFull, "%Y-%m-%d %H:%M:%S").timetuple()), slope, intercept), 0)

    # ----- LAST 6 RECORDS  -----

    data = [id]

    cur.execute("SELECT * FROM remplissage WHERE idparking = %s ORDER by date DESC LIMIT 6", data)

    lastRecords = []

    for i in cur:
        lastRecords.append(Parking(i[0], int(i[3]), str(datetime.strptime(str(i[1]), "%Y-%m-%d %H:%M:%S").strftime("%d/%m/%Y %H:%M:%S")), i[2], i[4]))

    return jsonify({'code': 200, 'data': {'estimate' : estimate, 'last_records': [record.serialize() for record in lastRecords]}})

def predict(x, slope, intercept):
   return slope * x + intercept

if __name__ == '__main__':
    app.run(host='0.0.0.0')