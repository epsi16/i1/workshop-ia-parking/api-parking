import psycopg2
from config import config

class database:

  def __init__(self):
      params = config()
      self.connection = psycopg2.connect(**params)
