# ----- Database -----
import psycopg2

# ----- Custom -----
from config import config

class Database:

  def __init__(self):
      params = config()
      self.connection = psycopg2.connect(**params)

class Parking:

  def __init__(self, id, id_nantes_metropole, date, nbr_places_occupees, nbr_places_max):
    self.id = id
    self.id_nantes_metropole = id_nantes_metropole
    self.date = date
    self.nbr_places_occupees = nbr_places_occupees
    self.nbr_places_max = nbr_places_max

  def serialize(self):
    return {
      'id': self.id,
      'id_nantes_metropole': self.id_nantes_metropole,
      'date': self.date,
      'nbr_places_occupees': self.nbr_places_occupees,
      'nbr_places_max': self.nbr_places_max,
    }