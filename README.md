# API Parking

**Requirements :**
- Python 3.7

**Setup guide :**

- Clone the repository
- Copy the *config.ini .dist* file and replace all environment variables by yours
- Execute the command :
```pip install -r requirements.txt```